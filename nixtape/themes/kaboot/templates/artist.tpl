{include file='header.tpl' subheader='artist-header.tpl'}

{if $flattr_uid}
{include file='flattr.tpl'}
{/if}

<div about="{$id}" typeof="mo:MusicArtist">

	<div class="vcard">

{* We load player in sidebar instead, see footer include at bottom of file
		{if $streamable}
		<div id='player-container'>
		{include file='player.tpl'}
		</div>
		{/if}
*}

		{if $bio_summary}
		<div class="note" id="bio" property="bio:olb" datatype="" style='clear: left;'>
		<h4>{t}Biography{/t}</h4>
		<p>{$bio_summary}</p>
			{if $bio_content}
				<a href='#' onclick='$("#show_more_bio").toggle(500); $("#bio_content").toggle(500);' id='show_more_bio'>{t}Show more...{/t}</a>
				<p id='bio_content' style='display: none;'>{$bio_content}</p>
			{/if}
		</div>
		{/if}
	</div>

	{include file='flattr-artist-button.tpl'}

	<h4>{t}Albums{/t}</h4>
	{artistalbums artist=$artist->name}
	{include file='albumlist.tpl' class=#table# items=$artistalbums fimage=true fstream=true ftime=true}
	{if $add_album_link}<a href='{$add_album_link}'><strong>{t}Add new album{/t}</strong></a>{/if}

	<h4>{t}Tracks{/t}</h4>
	{artisttracks artist=$artist->name}
	{include file='tracklist.tpl' class=#table# items=$artisttracks fimage=true fstream=true}

	<br />

	{if !empty($similarArtists)}
		<h3 style='text-align: center; clear: left;'>{t}Similar free artists{/t}</h3>
		<ul class="tagcloud">
		{section name=i loop=$similarArtists}
			<li style='font-size:{$similarArtists[i].size}'><a href='{$similarArtists[i].url}'>{$similarArtists[i].artist}</a></li>
		{/section}
		</ul>
	{/if}

	<br />

	{if !empty($tagcloud)}
		<h3 style='text-align: center; clear: left;'>{t}Tags used to describe this artist{/t}</h3>
		<ul class="tagcloud">
		{section name=i loop=$tagcloud}
			<li style='font-size:{$tagcloud[i].size}'><a href='{$tagcloud[i].pageurl}' title='{t uses=$tagcloud[i].count}This tag was used %1 times{/t}' rel='tag'>{$tagcloud[i].name}</a></li>
		{/section}
		</ul>
	{/if}

	<br />

</div>

{include file='footer.tpl' sideplayer=true}

