<div id="track-header">
	<img style="float:left;width:64px;height:64px;margin-right:15px;" {if $artist->image_small} src="{$artist->image_small}" {else} src="{$img_url}/qm160.png" {/if} />
	<div>
		<div class="music-header" style="border-bottom:1px solid rgb(221,221,221);width:calc(100% - 100px);display:inline-block;">
			<h3 style="display:inline;">{$track->name}</h3> by <a href="{$artist->getURL()}">{$artist->name}</a>
		</div>
		{include file='submenu.tpl'}
	</div>
</div>
