{if isset($submenu)}
	<ul class="inline submenu">
	{foreach $submenu as $i}
		<li {if $i.active}class='active'{/if}>
			<a href='{$i.url}'>{$i.name}</a>
		</li>
	{/foreach}
	</ul>
	{else}&nbsp;
{/if}
