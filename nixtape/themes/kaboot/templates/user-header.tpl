<div id="user-profile-header">
	<img style="float:left;width:64px;height:64px;margin-right:15px;" src="{$me->getAvatar()}" />
	<div>
		<div style="border-bottom:1px solid rgb(221,221,221);width:calc(100% - 100px);display:inline-block;">
			<h3 style="display:inline;">{$me->name}</h3>{if $me->homepage}<a href="{$me->homepage}">website</a>{/if}<span class="pull-right"><b>{$me->getTotalTracks()}</b> plays</span>
		</div>
		{include file='submenu.tpl'}
	</div>
	{if $me->bio && $showbio}
	<br/>
	<div id='bio'>
		<blockquote>{$me->bio|escape:'html':'UTF-8'}</blockquote>
	</div>
	{/if}
</div>
<br/>
