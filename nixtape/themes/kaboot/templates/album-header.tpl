<div id="album-header">
	<img style="float:left;width:64px;height:64px;margin-right:15px;" {if $album->image}src="{$album->image}"{else}src="{$img_url}/qm160.png"{/if} />
	<div>
		<div class="music-header" style="border-bottom:1px solid rgb(221,221,221);width:calc(100% - 100px);display:inline-block;">
			<h3 style="display:inline;">{$album->name}</h3> by <a href="{$artist->getURL()}">{$artist->name}</a>{if $album->releasedate}<span class="pull-right">released on <b>{$album->releasedate|date_format:"%Y-%m-%d"}</b></span>{/if}
		</div>
		{include file='submenu.tpl'}
	</div>
</div>
