{config_load file='theme.conf' scope='global'}
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	{if !($pagetitle)}
	<title>{$site_name}</title>
	{else}
	<title>{$pagetitle|escape:'html':'UTF-8'} &mdash; {$site_name}</title>
	{/if}
	<link rel="stylesheet" href="{$base_url}/themes/{$default_theme}/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="{$base_url}/themes/{$default_theme}/css/kaboot.css" type="text/css" />
	<script type="text/javascript" src="{$base_url}/js/jquery.min.js"></script>
	<script type="text/javascript" src="{$base_url}/js/jquery-ui.custom.min.js"></script>
	<script type="text/javascript" src="{$base_url}/themes/{$default_theme}/js/bootstrap.js"></script>
	<script type="text/javascript">
		var base_url="{$base_url}";
	</script>
	<script type="text/javascript" src="{$base_url}/js/player.js"></script>
	<meta name="author" content="FooCorp catalogue number FOO200 and contributors" />
{section name=i loop=$extra_head_links}
	<link rel="{$extra_head_links[i].rel|escape:'html':'UTF-8'}" href="{$extra_head_links[i].href|escape:'UTF-8'}" type="{$extra_head_links[i].type|escape:'html':'UTF-8'}" title="{$extra_head_links[i].title|escape:'html':'UTF-8'}"  />
{/section}
	<meta name="viewport" content="width=device-width,initial-scale=1">
</head>

<body>
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container">
				<a class="brand" href="{$base_url}">{$site_name}</a>
				<div class="nav-collapse collapse">
					{include file='menu.tpl'}
					{include file='search-box.tpl'}
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
		    <article>
				{if $nosidebar}
				<div class="span12">
				{else}
				<div class="span8">
				{/if}
					<header>
						{if isset($subheader)}
							{include file="$subheader"}
						{/if}
					</header>
					<section>
