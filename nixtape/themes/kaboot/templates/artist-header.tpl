<div id="artist-header">
	<img style="float:left;width:64px;height:64px;margin-right:15px;" {if $artist->image_small}src="{$artist->image_small}"{else}src="{$img_url}qm160.png"{/if} />
	<div>
		<div class="music-header" style="border-bottom:1px solid rgb(221,221,221);width:calc(100% - 100px);display:inline-block;">
			<h3 style="display:inline;">{$artist->name}</h3> {if $artist->homepage}<a href="{$artist->homepage}">website</a>{/if}<span class="pull-right"><b>{$artist->getListenerCount()}</b> listeners</span>
		</div>
		{include file='submenu.tpl'}
	</div>
</div>

